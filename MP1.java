import java.io.*;
import java.io.File;
import java.lang.reflect.Array;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.*;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;
// import com.google.common.collect.*;

public class MP1 {
    Random generator;
    String userName;
    String inputFileName;
    String delimiters = " \t,;.?!-:@[](){}_*/";
    String[] stopWordsArray = {"i", "me", "my", "myself", "we", "our", "ours", "ourselves", "you", "your", "yours",
            "yourself", "yourselves", "he", "him", "his", "himself", "she", "her", "hers", "herself", "it", "its",
            "itself", "they", "them", "their", "theirs", "themselves", "what", "which", "who", "whom", "this", "that",
            "these", "those", "am", "is", "are", "was", "were", "be", "been", "being", "have", "has", "had", "having",
            "do", "does", "did", "doing", "a", "an", "the", "and", "but", "if", "or", "because", "as", "until", "while",
            "of", "at", "by", "for", "with", "about", "against", "between", "into", "through", "during", "before",
            "after", "above", "below", "to", "from", "up", "down", "in", "out", "on", "off", "over", "under", "again",
            "further", "then", "once", "here", "there", "when", "where", "why", "how", "all", "any", "both", "each",
            "few", "more", "most", "other", "some", "such", "no", "nor", "not", "only", "own", "same", "so", "than",
            "too", "very", "s", "t", "can", "will", "just", "don", "should", "now"};

    void initialRandomGenerator(String seed) throws NoSuchAlgorithmException {
        MessageDigest messageDigest = MessageDigest.getInstance("SHA");
        messageDigest.update(seed.toLowerCase().trim().getBytes());
        byte[] seedMD5 = messageDigest.digest();

        long longSeed = 0;
        for (int i = 0; i < seedMD5.length; i++) {
            longSeed += ((long) seedMD5[i] & 0xffL) << (8 * i);
        }

        this.generator = new Random(longSeed);
    }

    Integer[] getIndexes() throws NoSuchAlgorithmException {
        Integer n = 10000;
        Integer number_of_lines = 50000;
        Integer[] ret = new Integer[n];
        this.initialRandomGenerator(this.userName);
        for (int i = 0; i < n; i++) {
            ret[i] = generator.nextInt(number_of_lines);
        }
        return ret;
    }

    public MP1(String userName, String inputFileName) {
        this.userName = userName;
        this.inputFileName = inputFileName;
    }

    static Map sortByValue(Map map) {
        List list = new LinkedList(map.entrySet());
        Collections.sort(list, new Comparator() {
            public int compare(Object o1, Object o2) {
                return 0 - ((Comparable) ((Map.Entry) (o1)).getValue())
                .compareTo(((Map.Entry) (o2)).getValue());
          }
        });

        Map result = new LinkedHashMap();
        for (Iterator it = list.iterator(); it.hasNext();) {
            Map.Entry entry = (Map.Entry)it.next();
            result.put(entry.getKey(), entry.getValue());
        }
        return result;
    }

    public String[] process() throws Exception {
        String[] ret = new String[20];
       
        //TODO
        Integer[] indexes = this.getIndexes();
        Integer lineNumber = 0;
        Map <String, Integer> wordMap = new HashMap<String, Integer>();

        try (BufferedReader br = new BufferedReader(new FileReader(this.inputFileName))) {
            String line;
            while ((line = br.readLine()) != null) {
                // select line on the indexes
                if (Arrays.asList(indexes).contains(lineNumber)) {
                    // tokenlize the line
                    StringTokenizer st = new StringTokenizer(line, this.delimiters);
                    while (st.hasMoreTokens()) {
                        String token = st.nextToken().toLowerCase();
                        // eliminate stop words
                        if (Arrays.asList(this.stopWordsArray).contains(token)) {
                            // System.out.println(token);
                            continue;
                        }
                        // counting the word
                        Integer count = wordMap.get(token);
                        if (count != null) {
                            wordMap.put(token, count+1);
                        } else {
                            wordMap.put(token, 1);
                        }
                        // System.out.println(st.nextToken());
                    }    
                }
                lineNumber++;
            }
        }
        // now sort
        // ValueComparator bvc = new ValueComparator(wordMap);
        // TreeMap sorted_map = new TreeMap(bvc);
        // sorted_map.putAll(wordMap);
        Map sortedMap = this.sortByValue(wordMap);

        System.out.println(wordMap);
        System.out.println("SORTED MAP");
        System.out.println(sortedMap);

        for ( String key : wordMap.keySet() ) {
            System.out.println( key );
        }
        return ret;
    }

    public static void main(String[] args) throws Exception {
        if (args.length < 1){
            System.out.println("MP1 <User ID>");
        }
        else {
            String userName = args[0];
            String inputFileName = "./input.txt";
            MP1 mp = new MP1(userName, inputFileName);
            String[] topItems = mp.process();
            for (String item: topItems){
                System.out.println(item);
            }
        }
    }
}
